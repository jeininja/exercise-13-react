import './App.css';
import SushiMenu from './components/SushiMenu';

function App() {

  return (
    <div>
      <SushiMenu />
    </div>
  );
}

export default App;
