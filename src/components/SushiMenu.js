import { Component } from "react";
import SushiCard from "./SushiCard";

class SushiMenu extends Component {
    //Each class component must have a constructor
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
        }
        /*this.fetchQuote = this.fetchQuote.bind(this);*/
    }

    //Only called when the component is being rendered for the first time
    //Meaning the first time the component has no values and it is being 
    //called again here to change the state
    async componentDidMount(){
        await this.fetchSushiCount()
    }

    //Fetches the sushi counts by setting the correct URL
    async fetchSushiCount() {
        try {
            //Base url for all the URLs
            let url = "https://sonic.dawsoncollege.qc.ca/~jaya/sushi/sushi.php";
            let urlObj = new URL(url)

            //https://sonic.dawsoncollege.qc.ca/~jaya/sushi/sushi.php?number=TYPE 
            //Returns the number of sushi in the category
            urlObj.searchParams.set("number", "nigiri");
            let response = await fetch(urlObj);
            if (response.ok) {
                let sushiCountJSON = await response.json();
                this.setState({count: sushiCountJSON.count})
            } else {
                throw new Error("Status code: " + response.status);
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return(
            //Create a SushiCard component for each index of the temporary array
            //The React way of iterating within the render method is using map
            //Notice we don't actually care about the values inside the array
            //We simply fill it with '0' and use the index(size)
            Array(this.state.count).fill(0).map( (num, index) => {
                return (
                   <SushiCard type="nigiri" number={index} key={index} /*getNew={this.fetchQuote}*//>
               );
            })
        )
    }
}


export default SushiMenu;