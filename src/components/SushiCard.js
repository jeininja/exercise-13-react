import { Component } from "react";

class SushiCard extends Component{
    //Each class component must have a constructor
    constructor(props){
        super(props);
        this.state = {
            description: "",
            imageURL: "",
        }
    }

    //Only called when the component is being rendered for the first time
    //Meaning the first time the component has no values and it is being 
    //called again here to change the state
    async componentDidMount(){
        await this.fetchSushiInfo()
    }

    //Fetch the sushi informations by setting the correct URL
    async fetchSushiInfo(){
        try{
            let imageBaseURL = "https://sonic.dawsoncollege.qc.ca/~jaya/sushi/";
            let url = new URL("https://sonic.dawsoncollege.qc.ca/~jaya/sushi/sushi.php");
            url.searchParams.set("category", this.props.type);
            url.searchParams.set("num", this.props.number);
            let response = await fetch(url);
            if(response.ok){
                let sushiInfoJSON = await response.json();
                this.setState({description: sushiInfoJSON.description, imageURL: imageBaseURL+sushiInfoJSON.imageURL})
            //Throws an error if the status code is not in the 2xx range
            }else{
                throw new Error("Status code: " + response.status);
            }
            //Catches the error if the promise ever fails
        }catch(e){
            console.log(e)
        }
    }

    render() {
        return(
            <article>
                <figure>
                    <img src={this.state.imageURL} alt="Empty"/>
                </figure>
                <section>
                    <p>{this.props.type}</p>
                    <p>{this.state.description}</p>
                </section>
            </article>
        );
    }
}

export default SushiCard;